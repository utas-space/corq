from asyncio import current_task

from sqlalchemy.ext.asyncio import AsyncSession, async_scoped_session, create_async_engine
from sqlalchemy.orm import declarative_base, sessionmaker

from corq.settings import Settings


settings = Settings()

engine = create_async_engine(
    f"{settings.db_dsn}?check_same_thread=false&uri=true", echo=False, future=True
)

async_session_factory = sessionmaker(
    engine, expire_on_commit=False, class_=AsyncSession, future=True
)
async_session = async_scoped_session(async_session_factory, scopefunc=current_task)

Base = declarative_base()
