from pydantic import AnyHttpUrl, AnyUrl, BaseSettings


class Settings(BaseSettings):
    host: AnyHttpUrl = "http://hex6.phys.utas.edu.au"
    port: int = 12000
    db_dsn: str = "sqlite+aiosqlite:///db.sqlite3"
    server_listen_host: str = "0.0.0.0"

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        env_prefix = "corq_"
