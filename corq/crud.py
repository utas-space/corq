from typing import Optional

import structlog

from sqlalchemy import func, or_, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import selectinload

from corq import models, schemas


logger = structlog.get_logger()


async def get_job(db: AsyncSession, job_id: int) -> Optional[models.Job]:
    result = await db.execute(
        select(models.Job).where(models.Job.id == job_id).options(selectinload(models.Job.sub_jobs))
    )
    return result.scalar()


async def get_jobs(db: AsyncSession) -> list[models.Job]:
    result = await db.execute(
        select(models.Job)
        # .order_by(models.Job.priority.desc(), models.Job.queued_datetime.asc())
        .options(selectinload(models.Job.sub_jobs))
    )
    return result.scalars().all()


async def get_queued_jobs(db: AsyncSession) -> list[models.Job]:
    result = await db.execute(
        select(models.Job)
        .where(
            or_(
                models.Job.status == models.JobStatus.QUEUED,
                models.Job.status == models.JobStatus.RUNNING,
            )
        )
        .order_by(models.Job.priority.desc(), models.Job.queued_datetime.asc())
        .options(selectinload(models.Job.sub_jobs))
    )
    return result.scalars().all()


async def get_finished_jobs(db: AsyncSession) -> list[models.Job]:
    result = await db.execute(
        select(models.Job)
        .where(
            or_(
                models.Job.status == models.JobStatus.CANCELLED,
                models.Job.status == models.JobStatus.ERROR,
                models.Job.status == models.JobStatus.DONE,
            )
        )
        .order_by(models.Job.finished_datetime.desc())
        .options(selectinload(models.Job.sub_jobs))
    )
    return result.scalars().all()


async def get_next_queued_job(db: AsyncSession) -> Optional[models.Job]:
    result = await db.execute(
        select(models.Job)
        .where(
            or_(
                models.Job.status == models.JobStatus.QUEUED,
                models.Job.status == models.JobStatus.RUNNING,
            )
        )
        .order_by(models.Job.priority.desc(), models.Job.queued_datetime.asc())
        .options(selectinload(models.Job.sub_jobs))
    )
    return result.scalars().first()


async def create_job(db: AsyncSession, job: schemas.JobCreate) -> Optional[models.Job]:
    db_job = models.Job(
        priority=job.priority,
        name=job.name,
        description=job.description,
        tags=job.tags,
        status=models.JobStatus.QUEUED,
        base_path=job.base_path,
    )
    db.add(db_job)
    await db.flush()
    logger.info("created job", id=db_job.id, name=db_job.name, priority=db_job.priority)
    # db_job id should now be filled
    for sj in job.sub_jobs:
        db_subjob = models.SubJob(
            job_id=db_job.id, name=sj.name, path=sj.path, status=models.JobStatus.QUEUED
        )
        db.add(db_subjob)
        await db.flush()
        logger.info("created subjob", id=db_subjob.id, name=db_subjob.id, path=db_subjob.path)
        db_logs = models.Logs(subjob_id=db_subjob.id)
        db.add(db_logs)
    return await get_job(db, db_job.id)


async def get_subjob(db: AsyncSession, subjob_id: int) -> Optional[models.SubJob]:
    result = await db.execute(
        select(models.SubJob)
        .where(models.SubJob.id == subjob_id)
        .options(selectinload(models.SubJob.logs), selectinload(models.SubJob.job))
    )
    return result.scalar()
