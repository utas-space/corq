from sqlalchemy.ext.asyncio import AsyncSession

from corq.database import async_session
from corq.scheduler import Scheduler


scheduler: Scheduler


def init_dependencies(init_scheduler: Scheduler):
    global scheduler
    scheduler = init_scheduler


# Database session dependency
async def get_db() -> AsyncSession:
    db: AsyncSession = async_session()
    try:
        yield db
    finally:
        await db.close()


# Scheduler dependency
def get_scheduler() -> Scheduler:
    return scheduler
