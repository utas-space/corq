from importlib.metadata import PackageNotFoundError, version

try:
    __version__ = version("corq")
except PackageNotFoundError:
    # package is not installed
    __version__ = "development"
