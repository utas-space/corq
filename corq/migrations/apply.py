import os

import alembic.config


def apply_migrations():
    here = os.path.dirname(os.path.abspath(__file__))
    alembic_args = ["-c", os.path.join(here, "alembic.ini"), "upgrade", "head"]
    alembic.config.main(argv=alembic_args)
