#!/usr/bin/env python

from pathlib import Path
from typing import Optional
from uuid import UUID

import requests
import typer

from typer import Argument, Option

from corq import __version__
from corq.cli import server

# from corq.models.jobs import Job, JobList, JobStatus, SubJob
from corq.schemas import JobCreate, SubJobCreate
from corq.settings import Settings


settings = Settings()

app = typer.Typer()

app.add_typer(server.app, name="server")


def version_callback(value: bool):
    if value:
        typer.echo(f"CorQ CLI Version: {__version__}")
        raise typer.Exit()


@app.callback()
def version(
    version: Optional[bool] = typer.Option(None, "-v", "--version", callback=version_callback)
):
    pass


# @app.command(help="export all jobs")
# def export(output: Path = Argument(..., help="Name of exported job file")):
#     res = requests.get(f"{settings.host}:{settings.port}/api/jobs")
#     with open(output, "x") as file:
#         file.write(res.text)


# @app.command(name="import", help="import jobs")
# def import_jobs(job_file: Path = Argument(..., help="Jobs to be imported")):
#     headers = {"Accept": "application/json", "Content-Type": "application/json"}
#     with open(job_file, "rb") as file:
#         res = requests.post(
#             f"{settings.host}:{settings.port}/api/job/batch-submit", data=file, headers=headers
#         )


# @app.command(name="query", help="query jobs")
# def query_jobs(
#     name: str = Option(None, "-n", "--name", help="Filter on job name"),
#     tags: list[str] = Option(None, "-t", "--tag", help="Filter on tags"),
# ):
#     res = requests.get(
#         f"{settings.host}:{settings.port}/api/job/query", params={"name": name, "tags": tags}
#     )
#     jobs = JobList.parse_raw(res.text).__root__
#     for j in jobs:
#         print(
#             f"[{j.uid}]: {j.name}, {j.status}, {j.tags} ({j.sub_jobs_complete()}/{j.sub_jobs_count()})"
#         )


# @app.command(name="job-status", help="query job status")
# def query_job_status(job_id: UUID = Argument(..., help="Job id to query")):
#     res = requests.get(f"{settings.host}:{settings.port}/api/job/{job_id}")
#     job = Job.parse_raw(res.text)
#     print(f"[{job.uid}]: {job.name}, <{job.status}>, {job.tags}")
#     for sj in job.sub_jobs:
#         print(f"\t[{sj.uid}]: {sj.name}, <{sj.status}>")


# @app.command(name="cancel", help="cancel job")
# def cancel_job(job_id: UUID = Argument(..., help="ID of job to cancel")):
#     res = requests.get(f"{settings.host}:{settings.port}/api/job/{job_id}/cancel")


# @app.command(name="cancel-subjob", help="cancel subjob")
# def cancel_subjob(
#     job_id: UUID = Argument(..., help="ID of parent job"),
#     subjob_id: UUID = Argument(..., help="ID of subjob to cancel"),
# ):
#     res = requests.get(
#         f"{settings.host}:{settings.port}/api/job/{job_id}/subjob/{subjob_id}/cancel"
#     )


# @app.command(name="delete", help="delete job")
# def delete_job(job_id: UUID = Argument(..., help="ID of job to delete")):
#     res = requests.get(f"{settings.host}:{settings.port}/api/job/{job_id}/delete")


@app.command(help="submit a new job")
def submit(
    name: Path = Argument(..., help="Base name of sub jobs"),
    priority: int = Option(0, "-p", "--priority", help="Job priority"),
    description: str = Option("", "-d", "--description", help="Job description"),
    tags: list[str] = Option(None, "-t", "--tag", help="Job tags (can specify multiple times)"),
):
    if tags is None:
        tags = []

    job_name = name.name
    base_dir = name.parent
    sub_job_inputs = base_dir.glob(f"{job_name}*.im")
    sub_job_inputs = sorted(sub_job_inputs, key=lambda sji: int(sji.stem.split("_")[-1]))
    sub_job_base = [sj.parent / sj.stem for sj in sub_job_inputs]
    sub_jobs: list[SubJobCreate] = []
    for sjd in sub_job_base:
        n = sjd.name.split(".")[0]
        sub_jobs.append(SubJobCreate(name=n, path=str(sjd.resolve())))
    job = JobCreate(
        priority=priority,
        name=job_name,
        description=description,
        sub_jobs=sub_jobs,
        tags=",".join(tags),
        base_path=str(base_dir.resolve()),
    )
    res = requests.post(f"{settings.host}:{settings.port}/api/job/submit", data=job.json())
    print(res)
    # print(job.json())


if __name__ == "__main__":
    app()
