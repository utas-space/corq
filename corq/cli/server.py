import asyncio
import logging
import os

from typing import Optional

import structlog
import typer

from aiorun import run
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from uvicorn import Config, Server

from corq import ROOT_DIR, __version__, dependencies, routers
from corq.database import engine
from corq.migrations import apply
from corq.models import Base
from corq.scheduler import Scheduler
from corq.settings import Settings


settings = Settings()

app = typer.Typer(help="server commands")

webapp = FastAPI(title="Correlator Queue", redoc_url=None)
webapp.mount("/static", StaticFiles(directory=ROOT_DIR / "static"), name="static")
webapp.include_router(routers.web.router)
webapp.include_router(routers.jobs.router)

logger = structlog.get_logger()


async def uvicorn_server():
    class CustomServer(Server):
        def install_signal_handlers(self):
            pass

    config = Config(
        app=webapp, host=settings.server_listen_host, port=settings.port, log_config=None
    )
    server = CustomServer(config)
    return asyncio.create_task(server.serve())


async def run_corq():
    logger.info("creating web task")
    web_task = uvicorn_server()
    logger.info("creating scheduler task")
    # create our scheduler
    scheduler = Scheduler()
    # init our dependencies with this
    dependencies.init_dependencies(init_scheduler=scheduler)
    # create the scheduler task
    scheduler_task = asyncio.create_task(scheduler.run())

    tasks = asyncio.gather(web_task, scheduler_task)
    logger.info("running tasks")
    await tasks


def configure_logging():
    timestamper = structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M:%S")
    pre_chain = [
        # Add the log level and a timestamp to the event_dict if the log entry
        # is not from structlog.
        structlog.stdlib.add_log_level,
        timestamper,
    ]
    logging.config.dictConfig(
        {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {
                "plain": {
                    "()": structlog.stdlib.ProcessorFormatter,
                    "processor": structlog.dev.ConsoleRenderer(colors=False),
                    "foreign_pre_chain": pre_chain,
                },
                "colored": {
                    "()": structlog.stdlib.ProcessorFormatter,
                    "processor": structlog.dev.ConsoleRenderer(colors=True),
                    "foreign_pre_chain": pre_chain,
                },
            },
            "handlers": {
                "default": {
                    "level": "DEBUG",
                    "class": "logging.StreamHandler",
                    "formatter": "colored",
                },
                "file": {
                    "level": "DEBUG",
                    "class": "logging.handlers.WatchedFileHandler",
                    "filename": "corq.log",
                    "formatter": "plain",
                },
            },
            "loggers": {
                "": {
                    "handlers": ["default", "file"],
                    "level": "INFO",
                    "propagate": True,
                },
            },
        }
    )
    structlog.configure(
        processors=[
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            timestamper,
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
        ],
        context_class=dict,
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
        cache_logger_on_first_use=True,
    )


@app.command(help="start the server")
def start():
    configure_logging()
    logger.info("applying migrations")
    apply.apply_migrations()
    run(run_corq())
