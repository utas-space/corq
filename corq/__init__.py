from pathlib import Path

from .__version__ import __version__

ROOT_DIR = Path(__file__).resolve().parent
