from fastapi import APIRouter, Depends, HTTPException, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from sqlalchemy.ext.asyncio import AsyncSession

from corq import ROOT_DIR, __version__, crud
from corq.dependencies import get_db


templates = Jinja2Templates(directory=ROOT_DIR / "templates")
templates.env.globals["version"] = __version__

router = APIRouter(
    include_in_schema=False,
)


@router.get("/", response_class=HTMLResponse)
async def read_root(request: Request, db: AsyncSession = Depends(get_db)):
    queued_jobs = await crud.get_queued_jobs(db)
    finished_jobs = await crud.get_finished_jobs(db)
    return templates.TemplateResponse(
        "index.html", {"request": request, "queued": queued_jobs, "finished": finished_jobs}
    )


@router.get("/job/{id}", response_class=HTMLResponse)
async def read_job_detail(request: Request, id: int, db: AsyncSession = Depends(get_db)):
    job = await crud.get_job(db, id)
    if job is None:
        raise HTTPException(404, "job not found")

    return templates.TemplateResponse("job.html", {"request": request, "job": job})


@router.get("/subjob/{id}", response_class=HTMLResponse)
async def read_sub_job_detail(request: Request, id: int, db: AsyncSession = Depends(get_db)):
    subjob = await crud.get_subjob(db, id)
    if subjob is None:
        raise HTTPException(404, "subjob not found")
    return templates.TemplateResponse("subjob.html", {"request": request, "subjob": subjob})
