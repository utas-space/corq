from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from corq import crud
from corq.dependencies import get_db, get_scheduler
from corq.scheduler import Scheduler
from corq.schemas import Job, JobCreate


router = APIRouter(
    prefix="/api",
)


@router.get("/jobs", response_model=list[Job])
async def get_jobs(db: AsyncSession = Depends(get_db)):
    return await crud.get_jobs(db)


# @router.get("/job/query", response_model=list[Job])
# async def query_jobs(name: Optional[str] = None, tags: Optional[list[str]] = Query(None)):
#     ret_jobs = scheduler.all_jobs.values()
#     if name is not None:
#         ret_jobs = [j for j in ret_jobs if name in j.name]
#     if tags is not None:
#         tag_set = set(tags)
#         ret_jobs = [j for j in ret_jobs if any(t in tag_set for t in j.tags)]

#     matching_jobs = [j.copy(exclude={"sub_jobs": {"__all__": {"result"}}}) for j in ret_jobs]
#     return matching_jobs


@router.get("/job/{id}", response_model=Job)
async def get_job_info(id: int, db: AsyncSession = Depends(get_db)):
    job = crud.get_job(db, job_id=id)
    if job is None:
        raise HTTPException(404, "job not found")
    return job


@router.post("/job/submit")
async def add_job(
    job: JobCreate,
    db: AsyncSession = Depends(get_db),
    scheduler: Scheduler = Depends(get_scheduler),
):
    await scheduler.add_job(db, job)


@router.get("/job/{id}/cancel")
async def cancel_job(id: int, scheduler: Scheduler = Depends(get_scheduler)):
    pass
    # if uid not in scheduler.all_jobs:
    #     raise HTTPException(404, "job not found")
    # await scheduler.cancel_job(uid)


@router.get("/subjob/{id}/cancel")
async def cancel_subjob(id: int, scheduler: Scheduler = Depends(get_scheduler)):
    pass
    # if job_uid not in scheduler.all_jobs:
    #     raise HTTPException(404, "job not found")
    # await scheduler.cancel_sub_job(job_uid, subjob_uid)


@router.get("/job/{id}/delete")
async def delete_job(id: int, scheduler: Scheduler = Depends(get_scheduler)):
    pass
    # if uid not in scheduler.all_jobs:
    #     raise HTTPException(404, "job not found")
    # await scheduler.delete_job(uid)
