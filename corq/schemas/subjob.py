from datetime import datetime
from typing import Optional

from corq.models.job import JobStatus
from corq.schemas.base import OrmBaseModel
from corq.schemas.logs import Logs


# shared properties
class SubJobBase(OrmBaseModel):
    name: str
    path: str


# SubJob create
class SubJobCreate(SubJobBase):
    pass


# DB class
class SubJobInDB(SubJobBase):
    id: int
    job_id: int
    start_datetime: Optional[datetime] = None
    end_datetime: Optional[datetime] = None
    return_code: Optional[int] = None
    logs: Optional[Logs] = None
    status: JobStatus


# Properties to return via API
class SubJob(SubJobInDB):
    pass
