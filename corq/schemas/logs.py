from corq.schemas.base import OrmBaseModel


# shared properties
class LogsBase(OrmBaseModel):
    stdout: str
    stderr: str
    errormon: str
    statemon: str


# DB class
class LogsInDB(LogsBase):
    id: int
    subjob_id: int


# Properties to return via API
class Logs(LogsInDB):
    pass
