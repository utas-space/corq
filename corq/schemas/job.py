from datetime import datetime
from typing import Optional

import structlog

from pydantic import Field

from corq.models.job import JobStatus
from corq.schemas.base import OrmBaseModel
from corq.schemas.subjob import SubJob, SubJobCreate


logger = structlog.get_logger()

# shared properties
class JobBase(OrmBaseModel):
    priority: int
    name: str
    description = ""
    tags: str = ""
    base_path: str = ""
    sub_jobs: list[SubJob]


# Job create
class JobCreate(JobBase):
    sub_jobs: list[SubJobCreate]
    pass


# DB class
class JobInDB(JobBase):
    id: int
    queued_datetime: datetime
    finished_datetime: Optional[datetime] = None
    status: JobStatus = JobStatus.UNDEFINED


# Properties to return via API
class Job(JobInDB):
    pass


class JobList(OrmBaseModel):
    __root__: list[Job]
