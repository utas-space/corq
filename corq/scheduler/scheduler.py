# import asyncio
# import shutil
# import signal

# from datetime import datetime
# from pathlib import Path
# from typing import Optional
# from uuid import UUID

# import structlog

# from corq.models.jobs import Job, JobStatus, SubJob, SubJobResult


# logger = structlog.get_logger()


# class Scheduler:

#     # job_queue: asyncio.PriorityQueue[Job]
#     queue: dict[UUID, Job]
#     finished: dict[UUID, Job]
#     error: dict[UUID, Job]
#     all_jobs: dict[UUID, Job]
#     current_job: Optional[Job]
#     current_task_list: list[asyncio.Task]

#     def __init__(self):
#         # self.job_queue = asyncio.PriorityQueue()
#         self.queue = {}
#         self.finished = {}
#         self.all_jobs = {}
#         self.current_job = None
#         self.current_task_list = []

#     def sort_job_queue(self):
#         self.queue = {k: v for k, v in sorted(self.queue.items(), key=lambda item: item[1])[::-1]}
#         logger.info("job queue sorted", job_count=self.jobs_to_run())

#     def sort_finished_queue(self):
#         self.finished = {
#             k: v for k, v in sorted(self.finished.items(), key=lambda item: item[1].end_date)
#         }
#         logger.info("finished queue sorted")

#     def batch_add_jobs(self, jobs: list[Job]):
#         for j in sorted(jobs)[::-1]:
#             self.add_job(j)
#         self.sort_finished_queue()

#     def add_job(self, job: Job):
#         if job.uid in self.all_jobs:
#             raise Exception("Job already exists")

#         self.all_jobs[job.uid] = job

#         if job.status == JobStatus.UNDEFINED:
#             # this is a new job
#             logger.info("received new job", name=job.name, priority=job.priority, id=str(job.uid))
#             self.queue_job(job)
#         else:
#             logger.info(
#                 "importing job",
#                 name=job.name,
#                 priority=job.priority,
#                 id=str(job.uid),
#                 status=job.status,
#             )
#             if job.status == JobStatus.QUEUED:
#                 self.queue_job(job)
#             elif job.status == JobStatus.RUNNING:
#                 logger.info(
#                     "job was previously running, resetting status", name=job.name, id=str(job.uid)
#                 )
#                 for sj in job.sub_jobs:
#                     if sj.status == JobStatus.RUNNING:
#                         logger.info(
#                             f"resetting subjob status to {JobStatus.QUEUED}",
#                             name=sj.name,
#                             id=str(sj.uid),
#                         )
#                         sj.status = JobStatus.QUEUED
#                     logger.info(
#                         f"resetting parent job status to {JobStatus.QUEUED}",
#                         name=job.name,
#                         id=str(job.uid),
#                     )
#                     job.status = JobStatus.QUEUED
#                 self.queue_job(job)
#             elif job.status in [JobStatus.DONE, JobStatus.ERROR, JobStatus.CANCELLED]:
#                 logger.info(
#                     "imported finished job", name=job.name, id=str(job.uid), status=job.status
#                 )
#                 self.finished[job.uid] = job

#     def queue_job(self, job: Job):
#         self.queue[job.uid] = job
#         self.sort_job_queue()
#         job.status = JobStatus.QUEUED
#         logger.info("added job to queue", name=job.name, priority=job.priority, id=str(job.uid))

#     async def cancel_sub_job(self, juid: UUID, sjuid: UUID):
#         if juid not in self.all_jobs:
#             raise Exception("Job does not exist")
#         job = self.all_jobs[juid]
#         sj = job.get_sub_job(sjuid)
#         if sj is None:
#             raise Exception("SubJob does not exist")
#         logger.info("cancelling subjob", name=sj.name, id=str(sj.uid))
#         if sj.status == JobStatus.RUNNING and self.current_job == job:
#             if len(self.current_task_list) > 0:
#                 logger.info("cancelling subjob tasks", name=sj.name, id=str(sj.uid))
#                 try:
#                     for t in self.current_task_list:
#                         t.cancel()
#                 except Exception as e:
#                     logger.error(e)
#         if sj.status == JobStatus.QUEUED or sj.status == JobStatus.RUNNING:
#             sj.status = JobStatus.CANCELLED

#     async def cancel_job(self, uid: UUID):
#         if uid not in self.all_jobs:
#             raise Exception("Job does not exist")
#         job = self.all_jobs[uid]
#         logger.info("cancelling job", name=job.name, id=str(job.uid))
#         if uid in self.queue:
#             del self.queue[uid]
#         job.status = JobStatus.CANCELLED
#         for sj in job.sub_jobs:
#             if sj.status == JobStatus.QUEUED or sj.status == JobStatus.RUNNING:
#                 await self.cancel_sub_job(uid, sj.uid)
#         self.finished[uid] = job

#     async def delete_job(self, uid: UUID):
#         await self.cancel_job(uid)
#         job = self.all_jobs[uid]
#         logger.info("deleting job", name=job.name, id=str(job.uid))
#         if uid in self.finished:
#             del self.finished[uid]
#         if uid in self.all_jobs:
#             del self.all_jobs[uid]

#     def jobs_to_run(self) -> int:
#         return len(self.queue)

#     async def run_sub_job(self, sub_job: SubJob):
#         logger.info("running subjob", name=sub_job.name, path=sub_job.path, id=str(sub_job.uid))

#         sub_job.start_time = datetime.utcnow()
#         sub_job.status = JobStatus.RUNNING
#         mpi_proc: Optional[asyncio.subprocess.Process] = None
#         errormon_proc: Optional[asyncio.subprocess.Process] = None

#         try:
#             difx_dir = Path(f"{sub_job.path}.difx")
#             if difx_dir.exists():
#                 logger.info(
#                     "found existing difx directory, removing",
#                     name=sub_job.name,
#                     path=sub_job.path,
#                     id=str(sub_job.uid),
#                 )
#                 shutil.rmtree(difx_dir)
#                 if difx_dir.exists():
#                     raise Exception(f"failed to remove existing difx diretory {difx_dir}")
#             mpi_proc = await asyncio.create_subprocess_exec(
#                 "spiralsmpi",
#                 "--machinefile",
#                 sub_job.path.parent / "machines",
#                 "--mca",
#                 "rmaps",
#                 "seq",
#                 "mpifxcorr",
#                 f"{sub_job.path}.input",
#                 stdout=asyncio.subprocess.PIPE,
#                 stderr=asyncio.subprocess.PIPE,
#             )
#             errormon_proc = await asyncio.create_subprocess_exec(
#                 "errormon2",
#                 stdout=asyncio.subprocess.PIPE,
#                 stderr=asyncio.subprocess.STDOUT,
#             )
#             mpi_task = asyncio.create_task(mpi_proc.communicate())
#             errormon_task = asyncio.create_task(errormon_proc.communicate())
#             self.current_task_list = [mpi_task, errormon_task]
#             done, pending = await asyncio.wait(
#                 [mpi_task, errormon_task], return_when=asyncio.tasks.FIRST_COMPLETED
#             )
#             if mpi_task in done:
#                 # everything is going to plan
#                 logger.info("mpifxcorr finished, cancelling errormon task")
#                 errormon_proc.send_signal(signal.SIGINT)
#                 # wait for the errormon_task to actually end
#                 await asyncio.wait_for(errormon_task, timeout=None)

#                 stdout, stderr = mpi_task.result()
#                 errormon_stdout, _ = errormon_task.result()
#                 filtered_errormon = [
#                     line
#                     for line in errormon_stdout.decode("utf-8").splitlines()
#                     if sub_job.name in line
#                 ]

#                 sub_job.end_time = datetime.utcnow()
#                 retcode = mpi_proc.returncode
#                 sub_job.result = SubJobResult(
#                     stdout=stdout.decode("utf-8").splitlines(),
#                     stderr=stderr.decode("utf-8").splitlines(),
#                     errormon=filtered_errormon,
#                     returncode=retcode,
#                 )
#                 if retcode == 0:
#                     logger.info(
#                         "subjob finished succesfully",
#                         name=sub_job.name,
#                         duration=(sub_job.end_time - sub_job.start_time),
#                         id=str(sub_job.uid),
#                     )
#                     sub_job.status = JobStatus.DONE
#                 else:
#                     logger.info(
#                         "subjob encountered an error",
#                         name=sub_job.name,
#                         duration=(sub_job.end_time - sub_job.start_time),
#                         id=str(sub_job.uid),
#                     )
#                     sub_job.status = JobStatus.ERROR
#                     logger.info(stderr)
#             # proc = await asyncio.create_subprocess_exec(
#             #     "echo",
#             #     f"{sub_job.path}.input",
#             #     stdout=asyncio.subprocess.PIPE,
#             #     stderr=asyncio.subprocess.PIPE,
#             # )
#             # proc = await asyncio.create_subprocess_shell(
#             #     f"sleep $[ ( $RANDOM % 30 )  + 1 ]s && echo {sub_job.path}.input",
#             #     stdout=asyncio.subprocess.PIPE,
#             #     stderr=asyncio.subprocess.PIPE,
#             # )
#         except Exception as e:
#             sub_job.end_time = datetime.utcnow()
#             logger.error(e)
#             if mpi_proc is not None:
#                 mpi_proc.terminate()
#             if errormon_proc is not None:
#                 errormon_proc.terminate()
#         finally:
#             if sub_job.status == JobStatus.RUNNING:
#                 sub_job.status = JobStatus.ERROR

#     async def run_job(self):
#         if self.current_job is not None:
#             raise Exception("found existing job")
#         logger.info("running job")
#         try:
#             job_key = next(iter(self.queue))
#             self.current_job = self.queue[job_key]
#             # self.current_job = self.job_queue.get_nowait()
#             self.current_job.status = JobStatus.RUNNING
#             sj = self.current_job.next_sub_job()
#             if sj is None:
#                 logger.info(
#                     "job finished", name=self.current_job.name, id=str(self.current_job.uid)
#                 )
#             else:
#                 logger.info(
#                     "found unfinished subjobs",
#                     name=self.current_job.name,
#                     subjob=sj.name,
#                     id=str(self.current_job.uid),
#                     subjob_id=str(sj.uid),
#                 )
#                 await self.run_sub_job(sj)
#         except Exception as e:
#             logger.error(e)
#         finally:
#             cj = self.current_job
#             if cj is not None:
#                 cj.update_status()
#                 if cj.complete():
#                     if cj.status == JobStatus.DONE:
#                         logger.info("job complete", name=cj.name, id=str(cj.uid))
#                     elif cj.status == JobStatus.ERROR:
#                         logger.info("job finished with errors", name=cj.name, id=str(cj.uid))
#                     del self.queue[cj.uid]
#                     self.finished[cj.uid] = cj
#                 else:
#                     logger.info(
#                         "job not finished, adding back to queue", name=cj.name, id=str(cj.uid)
#                     )
#                     # await self.job_queue.put(cj)
#                     self.queue[cj.uid] = cj
#                     self.sort_job_queue()
#                     cj.status = JobStatus.QUEUED
#                 self.current_job = None

#     async def run(self):
#         while True:
#             job_count = self.jobs_to_run()
#             if job_count > 0:
#                 await self.run_job()
#             await asyncio.sleep(1)
