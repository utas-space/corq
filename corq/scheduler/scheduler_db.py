import asyncio
import shutil
import signal

from datetime import datetime
from pathlib import Path
from typing import Optional

import structlog

from sqlalchemy.ext.asyncio import AsyncSession

from corq import crud
from corq.database import async_session
from corq.models import Job, JobStatus, SubJob
from corq.schemas import JobCreate


logger = structlog.get_logger()


class Scheduler:

    current_job: Optional[Job]
    current_task_list: list[asyncio.Task]
    db: AsyncSession

    def __init__(self):
        self.current_job = None
        self.current_task_list = []
        self.db = async_session()

    # async def batch_add_jobs(self, jobs: list[JobCreate]):
    #     for j in jobs:
    #         await self.add_job(j)

    async def add_job(self, db, job: JobCreate):
        logger.info("add_job called")
        async with db.begin():
            logger.info("sb session created")
            created_job = await crud.create_job(db, job)
        if created_job is None:
            logger.info("error adding job to queue", name=job.name, priority=job.priority)
        else:
            logger.info(
                "added job to queue",
                name=created_job.name,
                priority=created_job.priority,
                id=created_job.id,
            )

    async def cancel_sub_job(self, id: int):
        pass

    async def cancel_job(self, id: int):
        pass

    async def delete_job(self, id: int):
        pass

    async def run_sub_job(self, sub_job: SubJob):
        logger.info("running subjob", name=sub_job.name, path=sub_job.path, id=str(sub_job.id))

        async with self.db.begin():
            await self.db.refresh(sub_job)
            sub_job.start_time = datetime.utcnow()
            sub_job.status = JobStatus.RUNNING

        mpi_proc: Optional[asyncio.subprocess.Process] = None
        errormon_proc: Optional[asyncio.subprocess.Process] = None

        try:
            difx_dir = Path(f"{sub_job.path}.difx")
            if difx_dir.exists():
                logger.info(
                    "found existing difx directory, removing",
                    name=sub_job.name,
                    path=sub_job.path,
                    id=sub_job.id,
                )
                shutil.rmtree(difx_dir)
                if difx_dir.exists():
                    raise Exception(f"failed to remove existing difx diretory {difx_dir}")
            mpi_proc = await asyncio.create_subprocess_exec(
                "spiralsmpi",
                "--machinefile",
                Path(sub_job.path).parent / "machines",
                "--mca",
                "rmaps",
                "seq",
                "mpifxcorr",
                f"{sub_job.path}.input",
                cwd=Path(sub_job.path).parent,
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE,
            )
            errormon_proc = await asyncio.create_subprocess_exec(
                "errormon2",
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.STDOUT,
            )
            mpi_task = asyncio.create_task(mpi_proc.communicate())
            errormon_task = asyncio.create_task(errormon_proc.communicate())
            self.current_task_list = [mpi_task, errormon_task]
            done, pending = await asyncio.wait(
                [mpi_task, errormon_task], return_when=asyncio.tasks.FIRST_COMPLETED
            )
            if mpi_task in done:
                # everything is going to plan
                logger.info("mpifxcorr finished, cancelling errormon task")
                errormon_proc.send_signal(signal.SIGINT)
                # wait for the errormon_task to actually end
                await asyncio.wait_for(errormon_task, timeout=None)

                stdout, stderr = mpi_task.result()
                errormon_stdout, _ = errormon_task.result()
                filtered_errormon = [
                    line
                    for line in errormon_stdout.decode("utf-8").splitlines()
                    if sub_job.name in line
                ]

                async with self.db.begin():
                    sub_job = await crud.get_subjob(self.db, sub_job.id)  # get the logs as well
                    sub_job.end_time = datetime.utcnow()
                    sub_job.return_code = mpi_proc.returncode
                    sub_job.logs.stdout = stdout.decode("utf-8")
                    sub_job.logs.stderr = stderr.decode("utf-8")
                    sub_job.logs.errormon = "\n".join(filtered_errormon)

                    if sub_job.return_code == 0:
                        logger.info(
                            "subjob finished succesfully",
                            name=sub_job.name,
                            duration=(sub_job.end_time - sub_job.start_time),
                            id=sub_job.id,
                        )
                        sub_job.status = JobStatus.DONE
                    else:
                        logger.info(
                            "subjob encountered an error",
                            name=sub_job.name,
                            duration=(sub_job.end_time - sub_job.start_time),
                            id=sub_job.id,
                        )
                        sub_job.status = JobStatus.ERROR
                        logger.info(stderr)
        except Exception as e:
            async with self.db.begin():
                await self.db.refresh(sub_job)
                sub_job.end_time = datetime.utcnow()
            logger.error(e)
            if mpi_proc is not None:
                mpi_proc.terminate()
            if errormon_proc is not None:
                errormon_proc.terminate()
        finally:
            async with self.db.begin():
                await self.db.refresh(sub_job)
                if sub_job.status == JobStatus.RUNNING:
                    sub_job.status = JobStatus.ERROR

    async def run_job(self, job: Job):
        logger.info("running job", name=job.name, priority=job.priority, id=job.id)
        try:
            self.current_job = job
            async with self.db.begin():
                await self.db.refresh(self.current_job)
                self.current_job.status = JobStatus.RUNNING
            sj = self.current_job.next_sub_job()
            if sj is None:
                logger.info("job finished", name=self.current_job.name, id=self.current_job.id)
            else:
                logger.info(
                    "found unfinished subjobs",
                    name=self.current_job.name,
                    id=self.current_job.id,
                )
                await self.run_sub_job(sj)
        except Exception as e:
            logger.error(e)
        finally:
            cj = self.current_job
            if cj is not None:
                async with self.db.begin():
                    await self.db.refresh(cj)
                    cj.update_status()
                    if cj.is_complete():
                        if cj.status == JobStatus.DONE:
                            logger.info("job complete", name=cj.name, id=cj.id)
                        elif cj.status == JobStatus.ERROR:
                            logger.info("job finished with errors", name=cj.name, id=cj.id)
                    else:
                        logger.info("job not finished", name=cj.name, id=cj.id)
                        cj.status = JobStatus.QUEUED
                self.current_job = None

    async def run(self):
        while True:
            async with self.db.begin():
                next_job = await crud.get_next_queued_job(self.db)
            if next_job is not None:
                await self.run_job(next_job)
            await asyncio.sleep(1)
