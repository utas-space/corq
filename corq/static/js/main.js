var reloadTimeout;
var relCheckbox;

function setup_autoreload() {
    var rel = localStorage.getItem("reload");
    if (rel === null) {
        localStorage.reload = rel = 'true';
    }
    if (rel == 'true') {
        console.log("autoreload enabled");
        window.clearTimeout(reloadTimeout);
        reloadTimeout = window.setTimeout(() => {
            console.log("auto-reloading");
            window.location.reload();
        }, 10*1000);
    } else {
        console.log("autoreload disabled");
        window.clearTimeout(reloadTimeout);
    }
}

function toggle_autoreload() {
    var rel = localStorage.getItem("reload");
    rel = (rel === null || rel === 'true') ? 'false' : 'true'
    localStorage.setItem("reload", rel);
    setup_autoreload();
    update_reload_button();
}

function update_reload_button() {
    var rel = localStorage.getItem("reload");
    relCheckbox.checked = (rel === 'true') ? true : false;
}

function tweak_dates() {
    dayjs.locale('en-au')
    dayjs.extend(window.dayjs_plugin_utc)
    dayjs.extend(window.dayjs_plugin_timezone)
    dayjs.extend(window.dayjs_plugin_localizedFormat)
    dayjs.extend(window.dayjs_plugin_duration)
    dayjs.extend(window.dayjs_plugin_relativeTime)
    local_tz = dayjs.tz.guess()

    document.querySelectorAll(".utc-time").forEach((el) => {
        if (el.innerText === "") {
            return;
        }
        dt = dayjs.utc(el.innerText).tz(local_tz);
        el.innerText = dt.format('ll LTS'); 
        el.title = dayjs().to(dt)
    });
    document.querySelectorAll(".job-duration").forEach((el) => {
        if (el.innerText === "") {
            return;
        }
        dur = dayjs.duration(el.innerText, 'seconds');
        el.title = dur.format("HH:mm:ss");

        dt = parseFloat(el.innerText);
        if (dt < 60) {
            el.innerText = `${Math.round(dt)} seconds`
        } else if (dt < (3600)) {
            el.innerText = `${Math.round(dt / 60)} minutes`
        } else if (dt < (86400)) {
            el.innerText = `${Math.round(dt / 3600)} hours`
        }
        else {
            el.innerText = `${Math.round(dt / 86400)} days`
        }
    });
}

function highlight_status() {
    document.querySelectorAll(".job-table tbody tr").forEach((el) => {
        if (el.classList.contains("job-status-running")) {
            el.classList.add("table-primary");
        } else if (el.classList.contains("job-status-done")) {
            el.classList.add("table-success");
        } else if (el.classList.contains("job-status-error")) {
            el.classList.add("table-danger");
        } else if (el.classList.contains("job-status-cancelled")) {
            el.classList.add("table-warning");
        }
    });
}

document.addEventListener('DOMContentLoaded', () => {
    // don't set up autoreload if we are on a subjob page
    if (window.location.pathname.includes("subjob")) {
        relDiv = document.getElementById("divAutoReload");
        relDiv.style.display = "none";
    } else {
        relCheckbox = document.getElementById("switchAutoReload");
        relCheckbox.addEventListener('change', toggle_autoreload);
        setup_autoreload();
        update_reload_button();
    }
    tweak_dates();
    highlight_status();
});
