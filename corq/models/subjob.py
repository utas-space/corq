from typing import Optional

from sqlalchemy import Column, DateTime, Enum, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from corq.database import Base
from corq.models.job import JobStatus


class SubJob(Base):
    __tablename__ = "subjobs"

    id = Column(Integer, primary_key=True, index=True)
    job_id = Column(Integer, ForeignKey("jobs.id"))
    name = Column(String(100))
    path = Column(String(500))
    status = Column(Enum(JobStatus))
    start_datetime = Column(DateTime)
    end_datetime = Column(DateTime)
    return_code = Column(Integer)

    logs = relationship("Logs", back_populates="sub_job", uselist=False)
    job = relationship("Job", back_populates="sub_jobs")

    def is_complete(self):
        return (
            self.status == JobStatus.DONE
            or self.status == JobStatus.ERROR
            or self.status == JobStatus.CANCELLED
        )

    def duration(self) -> Optional[float]:
        if self.end_datetime is None or self.start_datetime is None:
            return None
        return (self.end_datetime - self.start_datetime).total_seconds()
