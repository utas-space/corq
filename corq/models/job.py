import datetime
import enum

from typing import Optional

import structlog

from sqlalchemy import Column, DateTime, Enum, Integer, String, Text
from sqlalchemy.orm import relationship

from corq.database import Base


logger = structlog.get_logger()


class JobStatus(str, enum.Enum):
    UNDEFINED = "undefined"
    QUEUED = "queued"
    RUNNING = "running"
    DONE = "done"
    ERROR = "error"
    CANCELLED = "cancelled"


class Job(Base):
    __tablename__ = "jobs"

    id = Column(Integer, primary_key=True, index=True)
    priority = Column(Integer)
    name = Column(String(100))
    description = Column(String(100))
    tags = Column(Text)
    status = Column(Enum(JobStatus))
    base_path = Column(String(500))
    queued_datetime = Column(DateTime, default=datetime.datetime.utcnow)
    finished_datetime = Column(DateTime)

    sub_jobs = relationship("SubJob", back_populates="job")

    def sub_jobs_count(self) -> int:
        return len(self.sub_jobs)

    def sub_jobs_complete(self) -> int:
        return len([sj for sj in self.sub_jobs if sj.is_complete()])

    def is_complete(self) -> bool:
        complete = True
        for sj in self.sub_jobs[::-1]:
            if complete == False:
                break  # short circuit
            complete = complete and sj.is_complete()
        return complete

    def next_sub_job(self) -> Optional["SubJob"]:
        jobs_to_run = [sj for sj in self.sub_jobs if sj.status == JobStatus.QUEUED]
        if len(jobs_to_run) == 0:
            return None
        else:
            return jobs_to_run[0]

    def update_status(self):
        done = True
        error = False
        for sj in self.sub_jobs:
            if sj.status == JobStatus.ERROR:
                error = True
            done = done and sj.is_complete()
        if done:
            if error:
                self.status = JobStatus.ERROR
            else:
                self.status = JobStatus.DONE
            self.finished_datetime = datetime.datetime.utcnow()
