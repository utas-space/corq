from sqlalchemy import Column, ForeignKey, Integer, Text
from sqlalchemy.orm import relationship

from corq.database import Base


class Logs(Base):
    __tablename__ = "logs"

    id = Column(Integer, primary_key=True, index=True)
    subjob_id = Column(Integer, ForeignKey("subjobs.id"), index=True)
    stdout = Column(Text)
    stderr = Column(Text)
    errormon = Column(Text)
    statemon = Column(Text)

    sub_job = relationship("SubJob", back_populates="logs")
